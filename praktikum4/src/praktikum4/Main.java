package praktikum4;
import java.util.Scanner;

class MotherBoard {
	// static nested class
	static class USB{
		int getTotalPorts(int usb2, int usb3){
			return usb2 + usb3;
		}
	}
}

public class Main {
	public static void main(String[] args) {
		int usb2, usb3;
		Scanner input = new Scanner(System.in);
		
		usb2 = input.nextInt();
		usb3 = input.nextInt();
		
		MotherBoard.USB usb = new MotherBoard.USB();
		
		System.out.println("Total Ports = "+usb.getTotalPorts(usb2, usb3));
	}
}